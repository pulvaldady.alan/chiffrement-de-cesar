#include<iostream>
#include<fstream>
#include"Cesar.h"

using namespace std;

int main()
{
    FILE *file_to_encrypt = fopen("not_encrypted.txt","r");
    if(file_to_encrypt==NULL){
        cout << "ERREUR OUVERTURE" << endl;
        return -1;
    }
    encrypt_cesar(file_to_encrypt,"encrypted.txt",1);
    fclose(file_to_encrypt);

    return 0;
}
